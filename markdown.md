name: inverse
layout: true
class: inverse

---
layout: false
background-image: url(img/czrnobyl1.jpg)
background-size: 80%
background-repeat: no-repeat
background-position: center top
class: bottom middle

# Typical errors in modular monoliths, synchronous microservices, event-driven systems

by Jakub Nabrdalik

---


# Explicitely optimize on defined attribute

--

Performance? Scalability? 

Most app require that you do not fuck it up, and that's it. 

In 2005 I single app in PHP (slowest tech ever) handled 200rps easily.

In 2015 2 cores of a shared server handled 6k rps (Allegro).

It's the stupid shit we do, that slows our systems down. 

Usually full-table-scans and lack of data locality.

If you don't know which attribute to optimize for, choose

*Maintainability* - how much time will people spend on keeping it alive

*Extensibility* - how easy will it be to add new features

--

...and if you are a startup

*Flexibility* - how much can we reuse when we do a pivot


---

# Definition of a module

--

A vertical slice (module has all layers: from DB access to API or even UI)

>  Because we tried 3-layered architecture and it ends up in mess

--

Encapsulation - everything apart from the API is hidden and inaccessible; clearly identifiable API and collaborators

> Because when all classes are public, we end up in spaghetti code

--

Module has its own database (at least tables/collections). Other modules cannot read/write data to that DB, they have to talk with that API

> Because when others write/read your database, you will be afraid to refactor (hard to verify). Integration through database is EVIL! SQL is inherently non-refactorizable.

--

Single responsibility: for a process (a verb), not data (noun)

> This requires explanation


---

layout: false
background-image: url(img/aUserModule.png)
background-size: 60%
background-repeat: no-repeat
background-position: center middle

# When module is responsible for data

---

layout: false
background-image: url(img/aNoCommunication.png)
background-size: 60%
background-repeat: no-repeat
background-position: center middle

# ...for process

---

layout: false
background-image: url(img/aQueryOrEvent.png)
background-size: 60%
background-repeat: no-repeat
background-position: center middle

# Let's add communication

---

layout: false
background-image: url(img/modesOfCommunication.png)
background-size: 70%
background-repeat: no-repeat
background-position: center middle

# How modules/microservices communicate


---

layout: false
background-image: url(img/aQueryOnly.png)
background-size: 60%
background-repeat: no-repeat
background-position: center middle

# Querry only

---


layout: false
background-image: url(img/aEventsOnly.png)
background-size: 60%
background-repeat: no-repeat
background-position: center middle

# Event driven

---

layout: false
background-image: url(img/aEventsOnly.png)
background-size: 50%
background-repeat: no-repeat
background-position: center right

# What about Data Duplication problem

--

First problem: size

> relevant only in extreme cases

--

Second problem: data consistency

> modular monolith: single TX

---

# Hey, those module look like microservices!

Sure, a module is often a candidate for a microservice

That's because of building system of systems - building blocks that do one things well.


#### The Unix philosophy 

by Doug McIlroy:

> Make each program do one thing well. To do a new job, build afresh rather than complicate old programs by adding new "features".


> Expect the output of every program to become the input to another, as yet unknown, program. 




---

background-image: url(img/query1.png)
background-size: 70%
background-repeat: no-repeat
background-position: center middle

# Query inefficiency

.footnote[
You cannot do a join query, because modules encapsulate their own data
]

---

background-image: url(img/query1.png)
background-size: 70%
background-repeat: no-repeat
background-position: center middle

# Query inefficiency


.footnote[
Monolith: Two very inefficient queries instead of a join

Microservices: Optimal number of remote calls: 0 (data locality)
]

???

Both: Loading all data into memory is inefficient. Cache has invalidation problem.


Search queries rent for availability and location for data. Filter is inefficient

Cache is a half way solution, but works only if you hit >80% of times
Invalidation problem

Remember: the best number of network calls when you answer a question is zero.

---

background-image: url(img/readmodel.png)
background-size: 70%
background-repeat: no-repeat
background-position: center middle

# Read model based on events

.footnote[
Data locality (0 outside calls)

Faster than joins even in a monolith (data optimized for query)
]

???

Local (inside module / microservice) read only representation of data, so that you can have data locality
Usually built on events
Faster than joins (denormalized) in a monolith
Required in distributed systems due to speed of light


---

layout: false
background-image: url(img/dataownership.png)
background-size: 70%
background-repeat: no-repeat
background-position: center middle

# Data ownership

.footnote[
Data ownership - this module controlls all the data about a car

You cannot store / process / make assumptions about the data
]

---

layout: false
background-image: url(img/singlepointoftruth.png)
background-size: 60%
background-repeat: no-repeat
background-position: center middle

# Single Point of Truth

.footnote[
Single point of truth: only I can say "car rented"

Nobody owns events. Events are what already happened (safe to duplicate)
]

---

# You can break Single Point of Truth

If you know what you are doing

---

layout: false
background-image: url(img/singlepointoftruth.png)
background-size: 50%
background-repeat: no-repeat
background-position: center right

# Single Point of Truth ensures invariants


.left-column[
Invariant
> we can only rent a car, that is not already rented

Implementation
> optimistic lock on DB inside Car Rental

If you need to provide an invariant on return: create a cycle, extract 3rd microservice or merge

Q: What if we want to see a table on UI with current car status and position?

A: Create a new module (dashboard) and feed it events

]

???

Changes to that data only happen in the module

The data is propagated to the rest of the system (via events or queries)

Every module/service can create a read model based on that data or cache it

To summarize: to provide invariants a given piece of data has to change in a single module (often with an optimistic lock or an upsert on DB). After the change is done, the fact of what happened belongs to everyone interested. 


---

layout: false
background-image: url(img/howsmall1.png)
background-size: 80%
background-repeat: no-repeat
background-position: center middle

# How small should a module be


---


layout: false
background-image: url(img/howsmall2.png)
background-size: 80%
background-repeat: no-repeat
background-position: center middle

# How small should a module be

--

.footnote[
If two steps of a process require different knowledge, and can be written by two developers working with a contract - split it into two modules

Small modules are easier to replace, refactor, debug. See [Unix philosophy](https://en.wikipedia.org/wiki/Unix_philosophy)
]

---

# Synchronous Microservices

Requirements by Thoughtworks:

- Independent deployments
- Products not Projects
- Smart endpoints and dumb pipes
- Decentralized Governance (self organized teams)
- Decentralized Data Management (no canonical data model, no central schemas)
- Infrastructure Automation (continuous deployment)
- Design for failure
- Evolutionary Design

(They actually started with async microservices!)

---

layout: false
background-image: url(img/splitapps1.png)
background-size: 80%
background-repeat: no-repeat
background-position: center middle

# Different quality requirements

--

.footnote[
When registration was opened for everyone born after 1981, the service died under DDOS, making it impossible for people to get vaccinated
]

---

layout: false
background-image: url(img/splitapps2.png)
background-size: 80%
background-repeat: no-repeat
background-position: center middle

# Different quality requirements

.footnote[
Split functions with different quality requirements into separate apps
]

---

# Availability

Availability in a monolith is often considered per application (a memory leak kills the whole JVM)

--

Think abotu SLA per process

--

You can minimize a failure impact by moving a module (process) into a separate microservice

---

layout: false
background-image: url(img/engineonfire.jpeg)
background-size: 70%
background-repeat: no-repeat
background-position: center middle

# Aviation

.footnote[
But aviation does it differently: planes have as little redundancy in engines as possible
It's easier to provide quality with less parts (but: engines do not change in flight)
]

???


To provide availability: should you split modules into separate apps, or limit the number of apps (things that can fail)?

---


layout: false
background-image: url(img/theinfluence.png)
background-size: 80%
background-repeat: no-repeat
background-position: center middle


---

# Availability recommendation

Avoid designing any apps that needs high availability and will have a lot of changes

High availability is much easier when devs do not introduce new bugs

Even old shitty monoliths can become stable (if you don't touch them)

Do not be afraid to split a module into two: 
- high SLA / low change frequency process
- low SLA / high change frequency process

[the paper](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/tr-2008-11.pdf)


---

# Eventual consistency

To scale infinitely we must avoid coordination

Life beyond distributed transactions: an Apostate's Opinion - Path Helland (Amazon)

We need at-least-once delivery guarantees

Don't be afraid. It's easy to achieve even without a message bus (see: transactional outbox pattern)

But if you have a message bus, you have more options. For a very good and easy to understand description of guarantees in Kafka see this [article](https://medium.com/@andy.bryant/processing-guarantees-in-kafka-12dd2e30be0e) by Andy Bryant

---

# No End-to-end tests 

> The only env similar to prod is prod. Mitigate with Consumer Driven Contracts

---

# If not reactive, sub optimual CPU usage

> How many developers are thread pool aware? 

> How often we define a java microservice with a single core?

> Who calculates RAM properly?

> Do not increase number of pods, before you utilize CPU/IO or RAM

--

### Questions before going to prod:

> what traffic (tps/rps) your app will survive with current configuration?

> what's the first bottleneck?

> what the action plan (best in README) when we have to improve performance at 3am on Sunday morning?

All these require firing up stress tests and monitoring

---

# Event Driven distributed system problems

--

Someone has to do a PhD on the message bus (closed source: even worse, providers lie a lot)

> Read "Designing Event-Driven Systems" - Ben Stopford (free pdf)

> You have to understand partitioning, consumer groups, compacted topics, Dead letter queue, Transactional outbox pattern

--

People create chaos:

> They do not understand public / team level / private events

> Event (Driven) != Event (Sourcing) != Event (Storming)

--

Event Driven event - communication pattern

Event Sourcing event - persistence pattern

Event Storming event - business event

May sometimes look identical, but doesn't have to.


---

layout: false
background-image: url(img/event.png)
background-size: 50%
background-repeat: no-repeat
background-position: center right

# Baseline

.left-column[
A baseline is a line that is a base for measurement or for construction.

When you find an error on producer side or do not have infinite event retention, you can send all the events again, to rebuild the read models in other modules/microservices. 

When the problem is on consumer side and you have infinite event retention (Kafka etc.) you may *just* move the offset to the beginning of queue and reprocess all events.

What if your consumers are idempotent? Clear all DBs?
]

---

# Kafka: what to remember

When you send an event to kafka, you get a completable future (nothing is sent right away: subscribe to error, or block)

--

You can use transactional outbox pattern, but understand the implications (performance, ordering)

--

Use @RetryableTopic, use DLT (if you can retry)

--

Do not stop processing a stream of events on first problem (unless you can stop the whole module and wait for a developer)

---

# Books to read

Software Architecture for Developers - Simon Brown

Fundamentals of Software Architecture: An Engineering Apporach - Mark Richards, Neal Ford

Software Architecture: The Hard Parts: Modern Trade-Off Analyses for Distributed Architectures - Neal Ford, Mark Richards, Pramod Sadalage, Zhamak Dehghani

Designing Event-Driven Systems - Ben Stopford

Reactive Design Patterns - Roland Kuhn Dr.

Team Topologies - Matthew Skelton, Manuel Pais

Domain-Driven Design: Tackling Complexity in the Heart of Software - Eric Evans


---

class: center middle


## Good luck

> Twitter @jnabrdalik

> jakubn@gmail.com
